package com.raizup.android.tennis.common;



import android.app.Application;
import android.content.res.Resources;
import android.util.Log;
import android.webkit.CookieManager;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.raizup.android.tennis.tennis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * Created by raizup on 2017-05-26.
 */

public class LoginManager  {

//    Context context;?
//    private final String root_url =  Resources.getSystem().getString(R.string.root_url);
//    private final String root_url = getString(R.string.root_url);
//    private final String root_url = "http://koreatennis.com/";
    private final String root_url = "http://koreatennis.com/";
    private final String mobile_url = root_url+"m/";



    private String authToken;
    private String mb_email;
    private String mb_id;
    private String mb_password;
    private String mb_name;
    private String area1;
    private String area2;
    private String club;
    private GoogleSignInAccount[] gsas;
    private String cookies;



    public LoginManager(){

    }

    public LoginManager(String login_info[]){
        this.mb_id = login_info[0];
        this.mb_password = login_info[1];
    }
    public LoginManager(String token){
        this.authToken = token;
    }

    public LoginManager(String mb_id, String mb_password){
        this.mb_id = mb_id;
        this.mb_password = mb_password;
    }



    public LoginManager(GoogleSignInAccount[] googleSignInAccounts) {
        this.gsas = googleSignInAccounts;
    }


    public JSONObject tokenCheker(String cookies) {

        this.cookies = cookies;
        JSONObject result = null;
        result = getResult(mobile_url+"tokenChecker.php", "", cookies);

        return result;
    }



    public JSONObject loginWithToken() {
        JSONObject result = null;
        try{
            String data ;
            data = URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode("tokenLogin","UTF-8");
            data += "&"+URLEncoder.encode("authToken","UTF-8")+"="+URLEncoder.encode(this.authToken,"UTF-8");

            Log.d(">>>data", data);
            result = getResult(mobile_url+"Auth.php", data);
        }catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return result;
    }

    public JSONObject loginWithEmail(){
        JSONObject result = null;
        try {
            String data;
            data = URLEncoder.encode("type","UTF-8");
            data += "="+URLEncoder.encode("emailLogin","UTF-8");
            data += "&"+URLEncoder.encode("mb_id","UTF-8");
            data += "="+URLEncoder.encode(this.mb_id,"UTF-8");
            data += "&"+URLEncoder.encode("mb_password","UTF-8");
            data += "="+URLEncoder.encode(this.mb_password, "UTF-8");

            Log.d(">>>data", data);
            result = getResult(mobile_url+"Auth.php", data);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public JSONObject loginWithGoogleId(){
        JSONObject result = null;
        try {
            String data;
            data = URLEncoder.encode("type","UTF-8");
            data += "="+URLEncoder.encode("googleLogin","UTF-8");
            data += "&"+URLEncoder.encode("mb_id","UTF-8");
            data += "="+URLEncoder.encode(this.gsas[0].getEmail(),"UTF-8");
            data += "&"+URLEncoder.encode("mb_name","UTF-8");
            data += "="+URLEncoder.encode(this.gsas[0].getFamilyName()+this.gsas[0].getGivenName(),"UTF-8");

            Log.d(">>>data", data);
            result = getResult(mobile_url+"Auth.php", data);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


//    public JSONObject registWithEmail() {
//        JSONObject result = null;
//        String data = null;
//        try {
//            data = URLEncoder.encode("type","UTF-8");
//            data += "="+URLEncoder.encode("emailLogin","UTF-8");
//            data += "&"+URLEncoder.encode("mb_id","UTF-8");
//            data += "="+URLEncoder.encode(this.mb_email,"UTF-8");
//            data += "&"+URLEncoder.encode("mb_password","UTF-8");
//            data += "="+URLEncoder.encode(this.mb_password,"UTF-8");
//            Log.e("auth parameter", data.toString());
//            JSONObject jsonobj = getResult(mobile_url+"Auth.php", data);
//            Log.e("result", jsonobj.toString());
//
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch(NullPointerException n){
//            n.printStackTrace();
//        }
//        return result;
//    }


    private JSONObject getResult(String url, String data){
        JSONObject jsonobject = null;
        try {
            CookieManager.getInstance().getCookie(root_url);
            URL tennisLoginoint = new URL(url);
            HttpURLConnection conn=(HttpURLConnection)  tennisLoginoint.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            final String COOKIES_HEADER = "Set-Cookie";

            Map<String, List<String>> headerFields = conn.getHeaderFields();
            List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

            if(cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                    String cookieName = HttpCookie.parse(cookie).get(0).getName();
                    String cookieValue = HttpCookie.parse(cookie).get(0).getValue();

                    String cookieString = cookieName + "=" + cookieValue;

                    CookieManager.getInstance().setCookie(root_url, cookieString);

                }
            }

            Log.d("conn.getResponseCode() ",conn.getResponseCode()+"" );
            if (conn.getResponseCode() == 200) {
                BufferedReader reader= new BufferedReader((new InputStreamReader(conn.getInputStream())));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine() )!= null){
                    sb.append(line+"\n");
                }
                Log.d("return !!!", sb.toString());
                try {
                    jsonobject = new JSONObject(sb.toString());
                    jsonobject .put("cookie", cookiesHeader);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonobject;
    }


    private JSONObject getResult(String url, String data, String cookies) {
        JSONObject jsonobject = null;
        try {
            CookieManager.getInstance().getCookie(root_url);
            URL tennisLoginoint = new URL(url);
            HttpURLConnection conn=(HttpURLConnection)  tennisLoginoint.openConnection();
            conn.setRequestProperty("Cookie", cookies);
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            final String COOKIES_HEADER = "Set-Cookie";

            Map<String, List<String>> headerFields = conn.getHeaderFields();
            List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

            Log.d("conn.getResponseCode() ",conn.getResponseCode()+"" );
            if (conn.getResponseCode() == 200) {
                BufferedReader reader= new BufferedReader((new InputStreamReader(conn.getInputStream())));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine() )!= null){
                    sb.append(line+"\n");
                }
                Log.d("return !!!", sb.toString());
                try {
                    jsonobject = new JSONObject(sb.toString());
                    jsonobject .put("cookie", cookiesHeader);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonobject;
    }

}
