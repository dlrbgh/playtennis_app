package com.raizup.android.tennis.common;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by raizup on 2017-05-27.
 */

public class Member  implements Serializable{
    private String mb_name;
    private String mb_id;
    private String mb_email;
    private String area1;
    private String area2;
    private String club;
    private String birthYear;
    private String grade;


    public Member(JSONObject jsonobj){
        try {
//            JSONObject mb = jsonobj.getJSONObject("mb");
            this.mb_name = jsonobj.getString("mb_name");
            this.mb_id = jsonobj.getString("mb_id");
            this.mb_email = jsonobj.getString("mb_email");
            this.area1 = jsonobj.getString("area1");
            this.area2 = jsonobj.getString("area2");
            this.club = jsonobj.getString("club");
            this.birthYear = jsonobj.getString("mb_4");
            this.grade = jsonobj.getString("mb_5");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getMb_name() {
        return mb_name;
    }

    public void setMb_name(String mb_name) {
        this.mb_name = mb_name;
    }

    public String getMb_id() {
        return mb_id;
    }

    public void setMb_id(String mb_id) {
        this.mb_id = mb_id;
    }

    public String getMb_email() {
        return mb_email;
    }

    public void setMb_email(String mb_email) {
        this.mb_email = mb_email;
    }

    public String getArea1() {
        return area1;
    }

    public void setArea1(String area1) {
        this.area1 = area1;
    }

    public String getArea2() {
        return area2;
    }

    public void setArea2(String area2) {
        this.area2 = area2;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
