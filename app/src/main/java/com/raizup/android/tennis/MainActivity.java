package com.raizup.android.tennis;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
//import com.naver.wcs.AndroidBridge;
import com.raizup.android.tennis.common.LoginManager;
import com.raizup.android.tennis.common.ObjectCarrier;
import com.raizup.android.tennis.common.Member;
import com.raizup.android.tennis.tennis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpCookie;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    //구글 애널리틱스
    private FirebaseAnalytics mFirebaseAnalytics;

    private boolean loginAgain = false;

    private boolean doubleBackToExitPressedOnce = false;

    AppCompatDialog progressDialog;

    private ImageView img_loading_frame;
    private  AnimationDrawable frameAnimation;

    private WebView mainWV;
    private ObjectCarrier cookieObjectCarrier;

    private ActionBarDrawerToggle mDrawerToggle;

    private Member mb;
    private EditText searchText;
    private ImageButton searchButton;
    private TextView mb_name;
    private TextView mb_email;
    private TextView area1;
    private TextView area2;
    private TextView club;
    private TextView grade;

    //nav header_mail menu
    private RelativeLayout goto_home_layout;
    private RelativeLayout myinfo_layout;
    private RelativeLayout favorite_layout;
    private RelativeLayout notification_layout;
    private RelativeLayout logout_layout;



    private LinearLayout grade_info_layout;

    private String presentUrl;

    private int resultCode;


    @SuppressLint({ "JavascriptInterface", "SetJavaScriptEnabled" })

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                // Do whatever you want here
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Do whatever you want here
            }
        };
        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Intent intent = getIntent();





    try {
        cookieObjectCarrier = (ObjectCarrier) intent.getSerializableExtra("cookie");
        List<String> cookiesHeader = (List<String>) cookieObjectCarrier.getObject();

        if (cookiesHeader != null) {
            for (String cookie : cookiesHeader) {
                String cookieName = HttpCookie.parse(cookie).get(0).getName();
                String cookieValue = HttpCookie.parse(cookie).get(0).getValue();
                String cookieString = cookieName + "=" + cookieValue;

                CookieManager.getInstance().setCookie(getString(R.string.mobile_url), cookieString);
            }
        }
        String mbString = intent.getExtras().getString("mb");
        try {
            JSONObject mbjson = new JSONObject(mbString);
            mb = new Member(mbjson);

            View v = navigationView.getHeaderView(0);
            searchText =  (EditText)v.findViewById(R.id.searchText);
            searchText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    //Enter key Action
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        String searchUrl =getString(R.string.searchUrl) ;
                        String search_text = searchText.getText().toString();
                        if(search_text.equals("")){
                            Toast.makeText(MainActivity.this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                            return false;
                        }else if(search_text.length() < 2){
                            Toast.makeText(MainActivity.this, "검색어를 두자 이상 입력해주세요.", Toast.LENGTH_SHORT).show();
                            return false;
                        }else{

                            Log.d("url>>>", searchUrl+"?st="+search_text);
                            mainWV.loadUrl(searchUrl+"?st="+search_text);
                            drawer.closeDrawer(GravityCompat.START);
                            return true;

                        }
                    }
                    return false;
                }
            });
            searchButton = (ImageButton) v.findViewById(R.id.searchButton);
            searchButton.setOnClickListener(
                    new ImageButton.OnClickListener() {
                        public void onClick(View v) {

                            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                            String searchUrl =getString(R.string.searchUrl) ;
                            String search_text = searchText.getText().toString();
                            if(search_text.equals("")){
                                Toast.makeText(MainActivity.this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                            }else if(search_text.length() < 2){
                                Toast.makeText(MainActivity.this, "검색어를 두자 이상 입력해주세요.", Toast.LENGTH_SHORT).show();
                            }else {
                                Log.d("url>>>", searchUrl + "?st=" + search_text);
                                mainWV.loadUrl(searchUrl + "?st=" + search_text);
                                drawer.closeDrawer(GravityCompat.START);
                            }
                        }
                    }
            );


            mb_name = (TextView) v.findViewById(R.id.mb_name);
            mb_email = (TextView) v.findViewById(R.id.mb_email);
            area1 = (TextView) v.findViewById(R.id.area1);
            area2 = (TextView) v.findViewById(R.id.area2);
            club = (TextView) v.findViewById(R.id.club);

            mb_name.setText(mb.getMb_name());
            mb_email.setText(mb.getMb_email());
            area1.setText(mb.getArea1());
            area2.setText(mb.getArea2());
            club.setText(mb.getClub());


            if(mb.getGrade() != null && ! mb.getGrade().equals("")){
                grade_info_layout = (LinearLayout) v.findViewById(R.id.grade_info_layout);
                grade_info_layout.setVisibility(View.VISIBLE);
                grade = (TextView) v.findViewById(R.id.grade);
                grade.setText(mb.getGrade());
            }


            goto_home_layout = (RelativeLayout) v.findViewById(R.id.goto_home_layout);
            myinfo_layout = (RelativeLayout) v.findViewById(R.id.myinfo_layout);
            favorite_layout = (RelativeLayout) v.findViewById(R.id.favorite_layout);
            notification_layout = (RelativeLayout) v.findViewById(R.id.notification_layout);
            logout_layout = (RelativeLayout) v.findViewById(R.id.logout_layout);

            goto_home_layout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    mainWV.loadUrl(getString(R.string.mobile_url));

                    InputMethodManager inputMethodManager = (InputMethodManager)  MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
            myinfo_layout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(MainActivity.this, LayerActivity.class);
                    intent.putExtra("cookie",cookieObjectCarrier);
                    intent.putExtra(getString(R.string.next_url), getString(R.string.update_member_profile_url));
                    startActivity(intent);

                }
            });
            favorite_layout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    mainWV.loadUrl(getString(R.string.mobile_url)+"competition_favor_list.php");
                    InputMethodManager inputMethodManager = (InputMethodManager)  MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
            notification_layout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(MainActivity.this, LayerActivity.class);
                    intent.putExtra("cookie",cookieObjectCarrier);
                    intent.putExtra(getString(R.string.next_url), getString(R.string.root_url)+"bbs/board.php?bo_table=notice&page=1&sst=wr_num%2Cwr_reply&page=1");
                    startActivity(intent);
                }
            });
            logout_layout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(MainActivity.this, LayerActivity.class);
                    intent.putExtra("cookie",cookieObjectCarrier);
                    intent.putExtra(getString(R.string.next_url), getString(R.string.update_member_profile_url));
                    startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }catch(Exception e){
        e.printStackTrace();
    }
        progressDialog = new AppCompatDialog(this);


        String nextUrl =intent.getExtras().getString(getString(R.string.next_url)) ;


        mainWV = (WebView) findViewById(R.id.mainWV);
        mainWV.getSettings().setJavaScriptEnabled(true);
        mainWV.addJavascriptInterface(new MyBridge(), "Android");
        mainWV.setWebViewClient(new WebViewClientClass(this));
//        mainWV.addJavascriptInterface(new AndroidBridge(this), "wcsa");
        mainWV.loadUrl(nextUrl);




    }


    @Override
    protected void onStart() {
        super.onStart();
        CookieManager.getInstance();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    public void onResume(){

        super.onResume();

        CookieManager.getInstance();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode > 0){
            mainWV.reload();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            InputMethodManager inputMethodManager = (InputMethodManager)  this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } else {
            if((!mainWV.getUrl().equals(getString(R.string.mobile_url))  ) && mainWV.canGoBack()){
                mainWV.goBack();
            }else if(mainWV.getUrl().equals(getString(R.string.email_login_url)) ){
                Intent intent = new Intent(MainActivity.this, SelectLoginActivity.class);
                startActivity(intent);
                finish();
            }else{
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "한번 더 누르면 앱이 종료됩니다", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
//                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //menu 를 사용하지않고 nav_header_maind에 모두 사용했으므로 사용하지않음
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = new Intent(MainActivity.this, LayerActivity.class);
        intent.putExtra("cookie",cookieObjectCarrier);
        switch (id){
            case R.id.profile:
                intent.putExtra(getString(R.string.next_url), getString(R.string.update_member_profile_url));
                startActivity(intent);
                break;
            case R.id.favorite:
                mainWV.loadUrl(getString(R.string.mobile_url)+"competition_favor_list.php");
                break;
            case R.id.notification:
                intent.putExtra(getString(R.string.next_url), getString(R.string.root_url)+"bbs/board.php?bo_table=notice&page=1&sst=wr_num%2Cwr_reply&page=1");
                startActivity(intent);
                break;
//            case R.id.logout:
//                intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url)+"clearSession.php");
//                startActivity(intent);
//                break;

        }

        View view = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class MyBridge{

        @JavascriptInterface
        public void showToast(String msg){
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void get_Member_info(String mb, String token){
            try {
                Log.d("getmemberinfo", mb);
                JSONObject mbjson = new JSONObject(mb);

                if(mb_name == null){

                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setNavigationItemSelectedListener(MainActivity.this);
                    View v = navigationView.getHeaderView(0);
                    mb_name = (TextView) v.findViewById(R.id.mb_name);
                    mb_email = (TextView) v.findViewById(R.id.mb_email);
                    area1 = (TextView) v.findViewById(R.id.area1);
                    area2 = (TextView) v.findViewById(R.id.area2);
                    club = (TextView) v.findViewById(R.id.club);
                }

//                Log.d("mgjson", mbjson.getString("mb_name"));


                SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("authToken", token);
                editor.commit();

                Log.d("main setting token", token);


                mb_name.setText( mbjson.getString("mb_name"));
                mb_email.setText(mbjson.getString("mb_email"));
                area1.setText(mbjson.getString("area1"));
                area2.setText(mbjson.getString("area2"));
                club.setText(mbjson.getString("club"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void openLayer(final String url, final String title){
            Log.d("openLayer","");

            Intent intent = new Intent(MainActivity.this, LayerActivity.class);
            intent.putExtra("cookie",cookieObjectCarrier);
            intent.putExtra(getString(R.string.next_url), url);
            startActivityForResult(intent, resultCode);

        }

        @JavascriptInterface
        //http://millky.com/@origoni/post/754?language=ko_kr
        public void openLnb(){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable(){
                        @Override
                        public void run() {
                            // 해당 작업을 처리함
                            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                            drawer.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }).start();
        }
    }
    public class WebViewClientClass extends WebViewClient {
        private Context context;
        public WebViewClientClass(Context context){
            this.context = context;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String  url) {


            if( url.startsWith("http:") || url.startsWith("https:") ) {
                return false;
            }
            return true;

        }


        @Override
        public void onPageStarted (WebView view,
                                   String url,
                                   Bitmap favicon){

            CookieManager.getInstance();
                try {
                    presentUrl = url;
                    SharedPreferences settings = getSharedPreferences("mysettings",
                            Context.MODE_PRIVATE);
                    String authToken = settings.getString("authToken", "");
//                    if(authToken.equals("")){
//                        String cookieStrings = CookieManager.getInstance().getCookie(getString(R.string.root_url));
//                        TokenChecker tc = new TokenChecker();
//                        JSONObject resultobj = tc.execute(cookieStrings).get();
//
//                        authToken = resultobj.getString("tmp_token");
//
//                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                        sp.edit().remove("authToken").commit();
//                        sp.edit().putString("authToken", authToken).commit();
//
//
//                        mb = new Member((JSONObject) resultobj.get("mb"));
//
//
//
//                        area1.setText(mb.getArea1());
//                        area2.setText(mb.getArea2());
//                        club.setText(mb.getClub());
//                    }else
                    if (url.equals(getString(R.string.step_2_update_url))) {

//                    String authToken = getSharedPreferences("authToken");
                        LoginProcess lp = new LoginProcess();
                        try {
                            JSONObject resultobj = lp.execute(authToken).get();
                            cookieObjectCarrier.setObject(resultobj.get("cookie"));
                            mb = new Member((JSONObject) resultobj.get("mb"));

                            area1.setText(mb.getArea1());
                            area2.setText(mb.getArea2());
                            club.setText(mb.getClub());

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loginAgain = false;
                    }

                    List<String> cookiesHeader = (List<String>) cookieObjectCarrier.getObject();

                    if (cookiesHeader != null) {
                        for (String cookie : cookiesHeader) {
                            String cookieName = HttpCookie.parse(cookie).get(0).getName();
                            String cookieValue = HttpCookie.parse(cookie).get(0).getValue();
                            String cookieString = cookieName + "=" + cookieValue;
                            CookieManager.getInstance().setCookie(getString(R.string.mobile_url), cookieString);
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
//                    SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = settings.edit();
//                    editor.putString("authToken", "");
//                    editor.commit();
//                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//                    sp.edit().remove("authToken").commit();
//                    sp.edit().putString("authToken", "").commit();
                }
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_loading);
            progressDialog.show();
            img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
            frameAnimation  = (AnimationDrawable) img_loading_frame.getBackground();
            img_loading_frame.post(new Runnable() {
                @Override
                public void run() {
                    frameAnimation.start();
                }
            });
        }


        @Override
        public void onPageFinished(WebView view, String url){
            Log.e(">>>eqal" , url+" "+getString(R.string.logout_after_forwarding_Url));
            if(url.equals(getString(R.string.logout_after_forwarding_Url))){

                SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("authToken", "");
                editor.commit();

                Intent intent = new Intent(MainActivity.this, SelectLoginActivity.class);
                startActivity(intent);
                finish();
            }else {
                try {
                    List<String> cookiesHeader = (List<String>) cookieObjectCarrier.getObject();

                    if (cookiesHeader != null) {
                        for (String cookie : cookiesHeader) {
                            String cookieName = HttpCookie.parse(cookie).get(0).getName();
                            String cookieValue = HttpCookie.parse(cookie).get(0).getValue();
                            String cookieString = cookieName + "=" + cookieValue;
                            Log.d("cookie", cookieString);
                            CookieManager.getInstance().setCookie(getString(R.string.mobile_url), cookieString);

                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                CookieManager.getInstance();
                progressDialog.setCancelable(false);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                progressDialog.setContentView(R.layout.progress_loading);
                img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
                frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
                img_loading_frame.post(new Runnable() {
                    @Override
                    public void run() {
                        frameAnimation.stop();
                    }
                });
                progressDialog.dismiss();
            }
        }
    }




    private class LoginProcess extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            LoginManager mb = new LoginManager(strings[0]);

            return mb.loginWithToken();
        }
    }
    private class TokenChecker extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... Strings) {
            LoginManager mb = new LoginManager();
            return mb.tokenCheker(Strings[0]);
        }

    }

}
