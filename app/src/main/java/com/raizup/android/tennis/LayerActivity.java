package com.raizup.android.tennis;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
//import com.naver.wcs.AndroidBridge;
import com.raizup.android.tennis.common.ObjectCarrier;
import com.raizup.android.tennis.tennis.R;

import java.net.HttpCookie;
import java.util.List;

public class LayerActivity extends AppCompatActivity {

    //구글 애널리틱스
    private FirebaseAnalytics mFirebaseAnalytics;


    private WebView layerWV;



    AppCompatDialog progressDialog;
    private ImageView img_loading_frame;
    private AnimationDrawable frameAnimation;
    private ObjectCarrier cookieObjectCarrier ;


    @SuppressLint({ "JavascriptInterface", "SetJavaScriptEnabled" })

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layer);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Intent intent = getIntent();
        try {
            cookieObjectCarrier = (ObjectCarrier) intent.getSerializableExtra("cookie");
            List<String> cookiesHeader = (List<String>) cookieObjectCarrier.getObject();

            if (cookiesHeader != null) {
                for (String cookie : cookiesHeader) {
                    String cookieName = HttpCookie.parse(cookie).get(0).getName();
                    String cookieValue = HttpCookie.parse(cookie).get(0).getValue();

                    String cookieString = cookieName + "=" + cookieValue;

                    CookieManager.getInstance().setCookie(getString(R.string.mobile_url), cookieString);

                }
            }
        }catch(NullPointerException e){
            e.printStackTrace();;
        }
        String url = intent.getExtras().getString(getString(R.string.next_url));


        layerWV = (WebView) findViewById(R.id.layerWV);
        layerWV.getSettings().setJavaScriptEnabled(true);
        layerWV.addJavascriptInterface(new MyBridge(), "Android");
//        layerWV.addJavascriptInterface(new AndroidBridge(this), "wcsa");
        layerWV.setWebViewClient(new WebViewClientClass(this));
        layerWV.loadUrl(url);


        progressDialog = new AppCompatDialog(this);
    }
    @Override
    public void onBackPressed() {
        if(layerWV.canGoBack()){
            layerWV.goBack();
        } else {
            super.onBackPressed();
//
        }
    }
    @Override
    public void finish() {
        Intent data = new Intent();
        setResult(1, data);

        super.finish();
    }

    private class MyBridge{

        @JavascriptInterface
        public void closeLayer(String tmp){
            finish();
        }

        @JavascriptInterface
        public void showToast(String msg){
            Toast.makeText(LayerActivity.this, msg, Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void logout(String nothing){
            new AlertDialog.Builder(LayerActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("로그아웃")
                    .setMessage("정말 로그아웃 하시겠습니까?")
                    .setPositiveButton("예", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("authToken", "");
                            editor.commit();
                            Intent intent = new Intent(LayerActivity.this, SplashActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    })
                    .setNegativeButton("아니오", null)
                    .show();
        }
    }


    public class WebViewClientClass extends WebViewClient {
        private Context context;
        public WebViewClientClass(Context context){
            this.context = context;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String  url) {


            if( url.startsWith("http:") || url.startsWith("https:") ) {
                return false;
            }
            return true;

        }


        @Override
        public void onPageStarted (WebView view,
                                   String url,
                                   Bitmap favicon){

            try{
                List<String> cookiesHeader = (List<String>) cookieObjectCarrier.getObject();

                if(cookiesHeader != null) {
                    for (String cookie : cookiesHeader) {
                        String cookieName = HttpCookie.parse(cookie).get(0).getName();
                        String cookieValue = HttpCookie.parse(cookie).get(0).getValue();
                        String cookieString = cookieName + "=" + cookieValue;
                        CookieManager.getInstance().setCookie(getString(R.string.mobile_url), cookieString);

                    }
                }
            }catch(NullPointerException e){
                e.printStackTrace();;
            }

//            CookieManager.getInstance();
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_loading);
            progressDialog.show();
            img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
            frameAnimation  = (AnimationDrawable) img_loading_frame.getBackground();
            img_loading_frame.post(new Runnable() {
                @Override
                public void run() {
                    frameAnimation.start();
                }
            });

        }


        @Override
        public void onPageFinished(WebView view, String url){
            CookieManager.getInstance();
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_loading);
            img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
            frameAnimation  = (AnimationDrawable) img_loading_frame.getBackground();
            img_loading_frame.post(new Runnable() {
                @Override
                public void run() {
                    frameAnimation.stop();
                }
            });
            progressDialog.dismiss();
        }
    }

}
