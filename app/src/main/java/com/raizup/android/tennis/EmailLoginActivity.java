package com.raizup.android.tennis;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.raizup.android.tennis.common.LoginManager;
import com.raizup.android.tennis.tennis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class EmailLoginActivity extends AppCompatActivity {

    EditText email;
    EditText password;

    Button login_button;
    Button regist_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.sub_actionbar);

        TextView title=(TextView)findViewById(getResources().getIdentifier("sub_actionbar", "id", getPackageName()));
        title.setText("이메일로그인");

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        regist_button = (Button)findViewById(R.id.regist_button);
        login_button = (Button)findViewById(R.id.login_button);

        regist_button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //Do stuff here
            }
        });
        login_button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                LoginProcess loginProcess = new LoginProcess();
                try {
                    String login_info[] = {email.getText().toString(), password.getText().toString()};

                    JSONObject loginResult = loginProcess.execute(login_info).get();
                    if(loginResult == null){
                        Toast.makeText(EmailLoginActivity.this, R.string.login_state_0000, Toast.LENGTH_SHORT).show();
                    }else if(loginResult.has("state")){
                        String state = loginResult.getString("state");
                        if(state.equals("failure")){
                            String msg = loginResult.getString("msg");
                            Toast.makeText(EmailLoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }else if(state.equals("ok")){
                            String process = loginResult.getString("process");
                            String token = loginResult.getString("authToken");

                            SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("authToken", token);
                            editor.commit();

//                        tttt = getSharedPreferences("authToken");


                            Log.d("processsss", process.equals(getString(R.string.google_login_regist_new))+"");
                            if(process.equals(getString(R.string.google_login_regist_new))
                                    || process.equals(getString(R.string.google_login_but_step2_empty))
                                    || process.equals(getString(R.string.google_login_return_token))  ){
                                // 추가입력하는곳으로 ㄱㄱ
                                Intent intent = new Intent(EmailLoginActivity.this, SplashActivity.class);
//                            intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url));
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(EmailLoginActivity.this, SplashActivity.class);
//                            intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url));
                                startActivity(intent);
                                finish();
                            }
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    private class LoginProcess extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String login_info[]) {

            LoginManager mb = new LoginManager(login_info);

            return mb.loginWithEmail();
        }
    }
}
