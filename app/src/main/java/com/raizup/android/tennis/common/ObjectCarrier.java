package com.raizup.android.tennis.common;

import java.io.Serializable;

/**
 * Created by raizup on 2017-05-27.
 */

public class ObjectCarrier implements Serializable {
    private Object obj;
    public void setObject(Object obj){
        this.obj = obj;
    }
    public Object getObject(){
        return this.obj;
    }
}
