package com.raizup.android.tennis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.CookieManager;

import com.google.firebase.analytics.FirebaseAnalytics;
//import com.naver.wcs.WCSLogEventAPI;
import com.raizup.android.tennis.common.ObjectCarrier;
import com.raizup.android.tennis.common.LoginManager;
import com.raizup.android.tennis.tennis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SplashActivity extends AppCompatActivity {


    //구글 애널리틱스
    private FirebaseAnalytics mFirebaseAnalytics;
    //s네이버 애널리틱스
    public Context wcsContext = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        wcsContext = this;
//        WCSLogEventAPI wcslog = WCSLogEventAPI.getInstance(wcsContext);
//        wcslog.onTrackSite((Activity)wcsContext, "SplashActivity");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
//                init();
                Intent intent;
                SharedPreferences settings = getSharedPreferences("mysettings",
                        Context.MODE_PRIVATE);
                String authToken = settings.getString("authToken", "");
                if(authToken.equals("")){
                    intent = new Intent(SplashActivity.this, SelectLoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    LoginProcess lp = new LoginProcess();
                    CookieManager.getInstance();
                    try {
                        JSONObject resultobj = lp.execute(authToken).get();
                        if(resultobj == null){
                            intent = new Intent(SplashActivity.this, SelectLoginActivity.class);

//                            SharedPreferences.Editor editor = settings.edit();
//                            editor.putString("authToken", authToken);
//                            editor.commit();

                            startActivity(intent);
                            finish();
                        }else {
                            String state = resultobj.getString("state");
                            if(state.equals("ok")){
                                authToken = resultobj.getString("authToken");

//                                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this.getApplicationContext());


//                                SharedPreferences.Editor editor = settings.edit();
//                                editor.putString("authToken", authToken);
//                                editor.commit();

                                ObjectCarrier cookieObjectCarrier = new ObjectCarrier();
                                cookieObjectCarrier.setObject(resultobj.get("cookie"));
                                intent = new Intent(SplashActivity.this, MainActivity.class);
                                intent.putExtra("cookie",cookieObjectCarrier);
                                intent.putExtra("mb",resultobj.get("mb").toString());
                                String process = resultobj.getString("process");
                                //more스텝이동이 이상함
                                //그냥 메인으로 이동하자.
                                if(process.equals(getString(R.string.google_login_but_step2_empty))){
                                    intent.putExtra(getString(R.string.next_url), getString(R.string.step_2_url));
                                }else{
                                    intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url));
                                }

                                startActivity(intent);
                                finish();
                            }else{
                                intent = new Intent(SplashActivity.this, SelectLoginActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        intent = new Intent(SplashActivity.this, SelectLoginActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        intent = new Intent(SplashActivity.this, SelectLoginActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        intent = new Intent(SplashActivity.this, SelectLoginActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }

            }
        }, 1000);// 3 초
    }


    private class LoginProcess extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            LoginManager mb = new LoginManager(strings[0]);
            return mb.loginWithToken();
        }
    }
}
