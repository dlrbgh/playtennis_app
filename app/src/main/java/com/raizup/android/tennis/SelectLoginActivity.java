package com.raizup.android.tennis;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
//import com.naver.wcs.WCSLogEventAPI;
import com.raizup.android.tennis.common.LoginManager;
import com.raizup.android.tennis.common.ObjectCarrier;
import com.raizup.android.tennis.tennis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SelectLoginActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {


    //구글 애널리틱스
    private FirebaseAnalytics mFirebaseAnalytics;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private static final String TAG = ">>>>>>>>>SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;

    private ImageButton emailBtn;
    private ImageButton googleBtn;
    public Context wcsContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_login);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        wcsContext = this;
//        WCSLogEventAPI wcslog = WCSLogEventAPI.getInstance(wcsContext);
//        wcslog.onTrackSite((Activity)wcsContext, "SplashActivity");
        
        mAuth = FirebaseAuth.getInstance();


        emailBtn = (ImageButton) findViewById(R.id.emailBtn);
        googleBtn = (ImageButton) findViewById(R.id.googleBtn);

        emailBtn.setOnClickListener(
                new ImageButton.OnClickListener() {
                    public void onClick(View v) {
                        Intent emailLoginIntent = new Intent(SelectLoginActivity.this, EmailLoginActivity.class);
                        startActivity(emailLoginIntent);
                        //Intent loginIntent = new Intent(SelectLoginActivity.this, MainActivity.class);
                        //loginIntent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url)+"login_mail.php");
                        //startActivity(loginIntent);
                        //finish();
                    }
                }
        );

        googleBtn .setOnClickListener(
                new ImageButton.OnClickListener() {
                    public void onClick(View v) {
                        mAuth.addAuthStateListener(mAuthListener);
                    }
                }
        );


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
//        mGoogleApiClient  = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
//                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getToken(true));

                } else {
                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    signIn();
                }
            }
        };
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.d(TAG, "onConnectionFailed:" + connectionResult);

    }

    //계정선택 액티비티에서 받은 정보
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(">>>", "여기아니냐");
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        Log.d("return code", requestCode+" "+RC_SIGN_IN);
        if (requestCode == RC_SIGN_IN) {
            Log.d("requestCode", requestCode+"");
            Log.d("resultCode", resultCode+"");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            Log.d("result isSuccess", result.isSuccess()+"");

            if (result.isSuccess()) {
                Log.d("시발성공했다","gpgpt");
                LoginProcess loginProcess = new LoginProcess();
                try {
                    JSONObject loginResult = loginProcess.execute(result.getSignInAccount()).get();
                    String state = loginResult.getString("state");

                    if(state.equals("ok")){
                        //로그인되면 이동해야지
//                        Log.d("login failure", "OKOK");
                        String process = loginResult.getString("process");
                        String token = loginResult.getString("authToken");

                        SharedPreferences settings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("authToken", token);
                        editor.commit();

//                        tttt = getSharedPreferences("authToken");


                        Log.d("processsss", process.equals(getString(R.string.google_login_regist_new))+"");
                        if(process.equals(getString(R.string.google_login_regist_new))
                                || process.equals(getString(R.string.google_login_but_step2_empty))
                                || process.equals(getString(R.string.google_login_return_token))  ){
                            // 추가입력하는곳으로 ㄱㄱ
                            Intent intent = new Intent(SelectLoginActivity.this, SplashActivity.class);
//                            intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url));
                            startActivity(intent);
                            finish();
                        }else{
                            //error
//                            Log.d("selectloginactivity","시발");
                            Toast.makeText(SelectLoginActivity.this, "계정정보를 가져오는데 실패했습니다\n관리자에게 문의하세요",
                                    Toast.LENGTH_SHORT).show();
                        }
//                        if(process.equals(getString(R.string.google_login_regist_new))
//                                ||process.equals(getString(R.string.google_login_but_step2_empty))  ){
//                            // 추가입력하는곳으로 ㄱㄱ
//
//                            ObjectCarrier cookieObjectCarrier = new ObjectCarrier();
//                            cookieObjectCarrier.setObject(loginResult.get("cookie"));
//                            Intent intent = new Intent(SelectLoginActivity.this, MainActivity.class);
//                            intent.putExtra("cookie",cookieObjectCarrier);
//                            intent.putExtra("mb",loginResult.get("mb").toString());
//                            intent.putExtra(getString(R.string.next_url), getString(R.string.step_2_url));
//                            startActivity(intent);
//                            finish();
//                        }else if(process.equals(getString(R.string.google_login_return_token))){
//                            //바로 메인페이지로
//                            Log.d("selectloginactivity", "main>>>");
//                            Intent intent = new Intent(SelectLoginActivity.this, MainActivity.class);
//                            intent.putExtra(getString(R.string.next_url), getString(R.string.mobile_url));
//                            startActivity(intent);
//                            finish();
//                        }else{
//                            //error
////                            Log.d("selectloginactivity","시발");
//                            Toast.makeText(SelectLoginActivity.this, "계정정보를 가져오는데 실패했습니다\n관리자에게 문의하세요",
//                                    Toast.LENGTH_SHORT).show();
//                        }

                    }else{
                        //안되면 띄워줘야지
                        Log.d("login failure", "bb");
                        Toast.makeText(SelectLoginActivity.this, "계정정보를 가져오는데 실패했습니다\n관리자에게 문의하세요",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("좆같네","tlqkf");
            }
        }
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private class LoginProcess extends AsyncTask<GoogleSignInAccount, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(GoogleSignInAccount... googleSignInAccounts) {

            LoginManager mb = new LoginManager(googleSignInAccounts);

            return mb.loginWithGoogleId();
        }
    }

}
